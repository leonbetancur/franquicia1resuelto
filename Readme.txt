Herramientas
Ejercicio Franquicia 1

Realizar Aplicativo que exponga servicios SOAP, para generar las tarjetas de cr�dito
de la franquicia.
Se deben exponer 3 servicios, crear tarjeta, registrar compra, consultar tarjeta.
La Aplicaci�n debe tener practicas t�cnicas, Integraci�n continua, Analis�s de c�digo est�tico, Cobertura.

Herramientas a Utilizar:

git
maven
eclipse
junit
mockito
hibernate
cxf (Servicios soap)
jenkins
sonar

descargar maven
adicionar al path


-Creamos el proyecto desde eclipse con maven
-->Configuramos el repositorio de arquetipos a
	http://repo.maven.apache.org/maven2/archetype-catalog.xml
-->Filtramos por cxf-jaxws-javafirst y elegimos el arquetipo con id org.apache.cxf.archetype

Group id -> co.com

Artifact id-> franquicia1


-creamos el repositorio

git init

-creamos archivo .gitignore
------
*.class
*.jar
/bin/
/build/
.settings/
.classpath
.project
/target
-----


git add .
git commit -m "primer commit"

creamos repositorio remoto en gitlab
adicionamos repositorio remoto

git remote add origin https://gitlab.com/leonbetancur/franquicia1resuelto.git

git push -u origin master

descargamos tomcat y lo adicionamos al eclipse

cambiamos el puerto 8080 por 8081, en D:\apache-tomcat-9.0.7-windows-x64\apache-tomcat-9.0.7\conf\server.xml

iniciamos el servidor, adicionamos el aplicativo y probamos por soapui



Instalamos jenkins

https://jenkins.io/download/

creamos el usuario 
admin
admin123

C:\Program Files (x86)\Jenkins\config   -> poner usersecurity en false

instalamos sonar version community

En Jenkins

Administrar Jenkins-> Global tool configuration->adiconar maven

instalar plugglin sonarqube scanner->adicionar sonar, adicionar lista pipeline
creamos la tareas de jenkisn

Crear nueva carpeta y crear tareas

propiedades sonar

sonar.projectKey : Franquicia1Resuelto
sonar.projectName= Franquicia1Resuelto
sonar.projectVersion=1.0

sonar.javasource=1.8
sonar.javatarget=1.8

sonar.sources=src/main/java
sonar.tests=src/test/java
sonar.java.binaries=target/classes/co/com/franquicia1resuelto
sonar.language=java

sonar.dynamicAnalysis=reuseReports
sonar.core.codeCoveragePlugin=jacoco
sonar.junit.reportsPath=target/surefire-reports/test
sonar.jacoco.reportPath=target/jacoco.exec


crear la interface para los servicios

se crea la logica sin bd, luego se crean los test

incluir mockito para los mocks

http://localhost:8081/Franquicia1Resuelto/HelloWorld?wsdl

patr�n test data builder

adicionar equals a tarjeta


persistencia

adicionamos las dependendecias de hibernate

creamos la base de datos
sudo mysql -u cursojava -p
create database franquicia1resuelto;

creamos el archivo persistence.xml en src/main/resources/META-INF

Ejercicio holabd


create table TARJETA_CURSO_JAVA(
	numero varchar(30) not null,
	usuario varchar(30),
	saldo decimal(10,2),
	deuda decimal(10,2),
	nombre varchar(50),
	vencimiento varchar(5),
	codigo_seguridad varchar(4),
	tipo_documento varchar(2),
	documento varchar(30)
);

create table HOLA_BD2(
	consecutivo int(11) auto_increment,
	mensaje varchar(30),
	PRIMARY KEY(consecutivo)
);
