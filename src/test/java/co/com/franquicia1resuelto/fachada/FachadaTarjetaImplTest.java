package co.com.franquicia1resuelto.fachada;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import co.com.franquicia1resuelto.datos.ITarjetaDao;
import co.com.franquicia1resuelto.dto.Cliente;
import co.com.franquicia1resuelto.dto.DatosCreacionTarjeta;
import co.com.franquicia1resuelto.dto.Tarjeta;
import co.com.franquicia1resuelto.servicios.ServicioTarjetas;

public class FachadaTarjetaImplTest {

	@InjectMocks
	private FachadaTarjetaImpl fachada;
	
	@Mock
	private ServicioTarjetas generadorTarjetas;
	@Mock
	private ITarjetaDao tarjetaDao;
	
	
	@Before
	public void iniciarTest() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testGenerarTarjeta() {
		//Arrange
		Cliente cliente = new Cliente();
		cliente.setApellidos("Betancur Herrera");
		cliente.setNombres("Le�n Andres");
		cliente.setTipoId("CC");
		cliente.setId("12498444");
		DatosCreacionTarjeta datosCreacionTarjeta = new DatosCreacionTarjeta();
		datosCreacionTarjeta.setBanco("Bancolombia");
		datosCreacionTarjeta.setCliente(cliente);
		datosCreacionTarjeta.setCupo(100000);
		
		Tarjeta tarjetaEsperada = new Tarjeta("LEON BETANCUR", "4551234557357", "01/20", "1234");
		
		Mockito.when(generadorTarjetas.generarTarjeta(Mockito.any(DatosCreacionTarjeta.class))).thenReturn(tarjetaEsperada);
		
		//Act
		Tarjeta tarjeta = fachada.generarTarjeta(datosCreacionTarjeta);
		
		//Assert
		assertEquals(tarjetaEsperada, tarjeta);
	}
}
