package co.com.franquicia1resuelto.servicios;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import co.com.franquicia1resuelto.dto.Cliente;
import co.com.franquicia1resuelto.dto.DatosCreacionTarjeta;
import co.com.franquicia1resuelto.dto.Tarjeta;
import co.com.franquicia1resuelto.testdatabuilder.ClienteTdb;
import co.com.franquicia1resuelto.testdatabuilder.DatosCreacionTarjetaTdb;

public class GeneradorTarjetasTest {
	
	private GeneradorTarjetas generadorTarjetas;
	
	@Before
	public void iniciarTest() {
		generadorTarjetas = new GeneradorTarjetas();
	}
	
	@Test
	public void testValidarNombre() {
		//Arrange
		Cliente cliente = new Cliente();
		cliente.setApellidos("Betancur Herrera");
		cliente.setNombres("Le�n Andres");
		cliente.setTipoId("CC");
		cliente.setId("12498444");
		DatosCreacionTarjeta datosCreacionTarjeta = new DatosCreacionTarjeta();
		datosCreacionTarjeta.setBanco("Bancolombia");
		datosCreacionTarjeta.setCliente(cliente);
		datosCreacionTarjeta.setCupo(100000);
		
		//Act
		Tarjeta tarjeta = generadorTarjetas.generarTarjeta(datosCreacionTarjeta);
		
		//Assert
		assertEquals("BETANCUR HERRERA LE�N ANDRES", tarjeta.getNombreTarjeta());
	}
	
	@Test
	public void testValidarNumero() {
		//Arrange
		Cliente cliente = new Cliente();
		cliente.setApellidos("Betancur Herrera");
		cliente.setNombres("Le�n Andres");
		cliente.setTipoId("CC");
		cliente.setId("12498444");
		DatosCreacionTarjeta datosCreacionTarjeta = new DatosCreacionTarjeta();
		datosCreacionTarjeta.setBanco("Bancolombia");
		datosCreacionTarjeta.setCliente(cliente);
		datosCreacionTarjeta.setCupo(100000);
		
		//Act
		Tarjeta tarjeta = generadorTarjetas.generarTarjeta(datosCreacionTarjeta);
		
		//Assert
		assertEquals(20, tarjeta.getNumero().length());
	}
	
	@Test
	public void testValidarNombreTdb() {
		//Arrange
		Cliente cliente = new ClienteTdb().withApellidos("PEREZ")
				                          .withNombres("FULANO")
				                          .build();
		DatosCreacionTarjeta datosCreacionTarjeta = new DatosCreacionTarjetaTdb().withCliente(cliente).build();
		
		//Act
		Tarjeta tarjeta = generadorTarjetas.generarTarjeta(datosCreacionTarjeta);
		
		//Assert
		assertEquals("PEREZ FULANO", tarjeta.getNombreTarjeta());
	}
	
	@Test
	public void testValidarNumeroTdb() {
		//Arrange
		DatosCreacionTarjeta datosCreacionTarjeta = new DatosCreacionTarjetaTdb().build();
		
		//Act
		Tarjeta tarjeta = generadorTarjetas.generarTarjeta(datosCreacionTarjeta);
		
		//Assert
		assertEquals(20, tarjeta.getNumero().length());
	}
	
	@Test(expected=ExcepcionCreacionTarjeta.class)
	public void testExcepcion() {
		//Arrange
		DatosCreacionTarjeta datosCreacionTarjeta = new DatosCreacionTarjetaTdb().withCupo(0).build();
		
		//Act
		Tarjeta tarjeta = generadorTarjetas.generarTarjeta(datosCreacionTarjeta);
		
	}
	

}
