package co.com.franquicia1resuelto.testdatabuilder;

import co.com.franquicia1resuelto.dto.Cliente;

public class ClienteTdb {
	private String tipoId;
	private String id;
	private String nombres;
	private String apellidos;
	
	public ClienteTdb() {
		this.tipoId = "CC";
		this.id = "12498444";
		this.nombres = "Le�n Andres";
		this.apellidos = "Betancur Herrera";
		System.out.println(this.toString());
	}

	public ClienteTdb withTipoId(String tipoId) {
		this.tipoId = tipoId;
		return this;
	}

	public ClienteTdb withId(String id) {
		this.id = id;
		return this;
	}

	public ClienteTdb withNombres(String nombres) {
		this.nombres = nombres;
		System.out.println(this.toString());
		return this;
	}

	public ClienteTdb withApellidos(String apellidos) {
		this.apellidos = apellidos;
		System.out.println(this.toString());
		return this;
	}
	
	public Cliente build() {
		System.out.println(this.toString());
		Cliente cliente = new Cliente();
		cliente.setApellidos(apellidos);
		cliente.setId(id);
		cliente.setNombres(nombres);
		cliente.setTipoId(tipoId);
		return cliente;
	}

	@Override
	public String toString() {
		return "ClienteTdb [tipoId=" + tipoId + ", id=" + id + ", nombres=" + nombres + ", apellidos=" + apellidos
				+ "]";
	}
	
	
}
