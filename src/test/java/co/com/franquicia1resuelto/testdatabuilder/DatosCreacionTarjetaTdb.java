package co.com.franquicia1resuelto.testdatabuilder;

import co.com.franquicia1resuelto.dto.Cliente;
import co.com.franquicia1resuelto.dto.DatosCreacionTarjeta;

public class DatosCreacionTarjetaTdb {

	private Cliente cliente;
	private String banco;
	private double cupo;
	
	public DatosCreacionTarjetaTdb() {
		cliente = new Cliente();
		cliente.setApellidos("Betancur Herrera");
		cliente.setNombres("Le�n Andres");
		cliente.setTipoId("CC");
		cliente.setId("12498444");
		this.banco = "Bancolombia";
		this.cupo = 100000;

	}
	
	public DatosCreacionTarjetaTdb withCliente(Cliente cliente) {
		this.cliente = cliente;
		return this;
	}
	public DatosCreacionTarjetaTdb withBanco(String banco) {
		this.banco = banco;
		return this;
	}
	public DatosCreacionTarjetaTdb withCupo(double cupo) {
		this.cupo = cupo;
		return this;
	}
	
	public DatosCreacionTarjeta build() {
		DatosCreacionTarjeta datosCreacionTarjeta = new DatosCreacionTarjeta();
		datosCreacionTarjeta.setBanco(banco);
		datosCreacionTarjeta.setCliente(cliente);
		datosCreacionTarjeta.setCupo(cupo);
		return datosCreacionTarjeta;
	}
}
