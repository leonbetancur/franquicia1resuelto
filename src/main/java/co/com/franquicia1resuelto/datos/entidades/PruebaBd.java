package co.com.franquicia1resuelto.datos.entidades;

import javax.persistence.EntityManager;

import co.com.franquicia1resuelto.datos.PersistenceManager;

public class PruebaBd {

	public static void main(String...args) {
	    HolaBd holaBd = new HolaBd();
	    holaBd.setMensaje("tercer mensaje");
	   
	    EntityManager em = PersistenceManager.INSTANCE.getEntityManager();

	    em.getTransaction()

	        .begin();

	    em.persist(holaBd);
	    HolaBd holaDesdeBd = em.find(HolaBd.class, 1);
	    if(holaDesdeBd!=null) {
	    	System.out.println(holaDesdeBd.getMensaje());
	    	holaDesdeBd.setMensaje("primer mensaje modificado");
	    	em.remove(holaDesdeBd);
	    }
	    

	    em.getTransaction()

	        .commit();

	    em.close();

	    PersistenceManager.INSTANCE.close();
	}
	
}
