package co.com.franquicia1resuelto.datos;


import co.com.franquicia1resuelto.datos.entidades.TarjetaCursoJava;

public interface ITarjetaDao {
	
	public void adicionarTarjeta(TarjetaCursoJava tarjetaCursoJava);
	
	public void actualizarTarjeta(TarjetaCursoJava tarjetaCursoJava);

	public TarjetaCursoJava tarjetaPorNumero(String numero);
}
