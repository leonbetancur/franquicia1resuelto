package co.com.franquicia1resuelto.datos;

import javax.persistence.EntityManager;

import co.com.franquicia1resuelto.datos.entidades.TarjetaCursoJava;

public class TarjetaDaoImpl implements ITarjetaDao {
	
	EntityManager em = PersistenceManager.INSTANCE.getEntityManager();
	
	@Override
	public void adicionarTarjeta(TarjetaCursoJava tarjetaCursoJava) {
		try {
			em.getTransaction().begin();
			em.persist(tarjetaCursoJava);
			em.getTransaction().commit();	
		}catch(Exception e) {
			em.getTransaction().rollback();
		}
		
	}

	@Override
	public void actualizarTarjeta(TarjetaCursoJava tarjetaCursoJava) {
		try {
			em.getTransaction().begin();
			em.persist(tarjetaCursoJava);
			em.getTransaction().commit();	
		}catch(Exception e) {
			em.getTransaction().rollback();
		}
	}

	@Override
	public TarjetaCursoJava tarjetaPorNumero(String numero) {
		return em.find(TarjetaCursoJava.class, numero);
	}

	

}
