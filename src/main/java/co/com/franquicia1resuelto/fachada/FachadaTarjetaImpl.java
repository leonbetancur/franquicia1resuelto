package co.com.franquicia1resuelto.fachada;

import java.util.logging.Logger;

import javax.jws.WebService;

import co.com.franquicia1resuelto.datos.ITarjetaDao;
import co.com.franquicia1resuelto.datos.TarjetaDaoImpl;
import co.com.franquicia1resuelto.dto.Compra;
import co.com.franquicia1resuelto.dto.DatosCreacionTarjeta;
import co.com.franquicia1resuelto.dto.Tarjeta;
import co.com.franquicia1resuelto.servicios.GeneradorTarjetas;
import co.com.franquicia1resuelto.servicios.ServicioTarjetas;

@WebService(endpointInterface = "co.com.franquicia1resuelto.fachada.FachadaTarjeta")
public class FachadaTarjetaImpl implements FachadaTarjeta {

	private static final Logger LOG = Logger.getLogger("FachadaTarjetaImpl");
	private ServicioTarjetas servicioTarjetas;
	private ITarjetaDao tarjetaDao;
	
	
	public FachadaTarjetaImpl() {
		tarjetaDao = new TarjetaDaoImpl();
		servicioTarjetas = new ServicioTarjetas(new GeneradorTarjetas(), tarjetaDao);
	}
	
	public Tarjeta generarTarjeta(DatosCreacionTarjeta datosCreacionTarjeta) {
		LOG.info("Ingreso a generar tarjeta, datos:"+datosCreacionTarjeta.toString());
		return servicioTarjetas.generarTarjeta(datosCreacionTarjeta);
	}

	@Override
	public boolean registrarCompra(Compra compra) {
		return servicioTarjetas.registrarCompra(compra);
	}

	@Override
	public Tarjeta consultarTarjeta(String numero) {
		return servicioTarjetas.consultarTarjeta(numero);
	}

}
