package co.com.franquicia1resuelto.fachada;

import javax.jws.WebService;

import co.com.franquicia1resuelto.dto.Compra;
import co.com.franquicia1resuelto.dto.DatosCreacionTarjeta;
import co.com.franquicia1resuelto.dto.Tarjeta;

@WebService
public interface FachadaTarjeta {
	public Tarjeta generarTarjeta(DatosCreacionTarjeta datosCreacionTarjeta) ;
	public boolean registrarCompra(Compra compra);
	public Tarjeta consultarTarjeta(String numero);
}
