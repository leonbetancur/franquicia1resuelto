package co.com.franquicia1resuelto.dto;

public class DatosCreacionTarjeta {

	private Cliente cliente;
	private String Banco;
	private double cupo;
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public String getBanco() {
		return Banco;
	}
	public void setBanco(String banco) {
		Banco = banco;
	}
	public double getCupo() {
		return cupo;
	}
	public void setCupo(double cupo) {
		this.cupo = cupo;
	}
	@Override
	public String toString() {
		return "DatosCreacionTarjeta [cliente=" + cliente + ", Banco=" + Banco + ", cupo=" + cupo + "]";
	}
	
	
}
