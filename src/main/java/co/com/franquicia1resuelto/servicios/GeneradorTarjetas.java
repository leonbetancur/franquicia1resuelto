package co.com.franquicia1resuelto.servicios;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import co.com.franquicia1resuelto.dto.DatosCreacionTarjeta;
import co.com.franquicia1resuelto.dto.Tarjeta;


public class GeneradorTarjetas {
	
	private static final String RIMEROS_NUMEROS_FRANQUICIA = "4982";
	private static final int DIGITOS_GENERAR = 16;
	private static final int VIGENCIA = 4;
	private static final int DIGITOS_SEGURIDAD = 4;
	Random random = new Random();
	
	public Tarjeta generarTarjeta(DatosCreacionTarjeta datosCreacionTarjeta) {
		validarDatosCreacion(datosCreacionTarjeta);
		String nombreTarjeta = generarNombreTarjeta(datosCreacionTarjeta);
		String numero = generarNumero();
		String vencimiento = generarFechaVencimiento();
		String codigoSeguridad = generarCodigoSeguridad();
		return new Tarjeta(nombreTarjeta, numero, vencimiento, codigoSeguridad);
	}
	
	private String generarNumero() {
		StringBuilder numero = new StringBuilder(RIMEROS_NUMEROS_FRANQUICIA);
		for(int i = 1; i<=DIGITOS_GENERAR; i++) {
			int numeroRandom = random.nextInt(9);
			numero.append(Integer.toString(numeroRandom));
		}
		return numero.toString();
	}
	
	private String generarCodigoSeguridad() {
		StringBuilder numero = new StringBuilder();
		for(int i = 1; i<=DIGITOS_SEGURIDAD; i++) {
			int numeroRandom = random.nextInt(9);
			numero.append(Integer.toString(numeroRandom));
		}
		return numero.toString();
	}
	
	private String generarNombreTarjeta(DatosCreacionTarjeta datosCreacionTarjeta) {
		return (datosCreacionTarjeta.getCliente().getApellidos()+" "+datosCreacionTarjeta.getCliente().getNombres()).toUpperCase();
	}
	
	private String generarFechaVencimiento() {
		LocalDate localDateHoy = LocalDate.now();
		LocalDate localDateVencimiento =  localDateHoy.plusYears(VIGENCIA);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/yy");
		return localDateVencimiento.format(formatter);
	}
	
	private void validarDatosCreacion(DatosCreacionTarjeta datosCreacionTarjeta) {
		if(datosCreacionTarjeta.getBanco()==null) {
			throw new ExcepcionCreacionTarjeta("Debe definir un banco");
		}
		if(datosCreacionTarjeta.getCupo()<=0) {
			throw new ExcepcionCreacionTarjeta("El cupo debe ser mayor a cero");
		}
		if(datosCreacionTarjeta.getCliente()==null) {
			throw new ExcepcionCreacionTarjeta("Debe definir los datos del cliente");
		}
		if(datosCreacionTarjeta.getCliente().getApellidos()==null) {
			throw new ExcepcionCreacionTarjeta("Debe definir los apellidos del cliente");
		}
		if(datosCreacionTarjeta.getCliente().getNombres()==null) {
			throw new ExcepcionCreacionTarjeta("Debe definir los nombres del cliente");
		}
		if(datosCreacionTarjeta.getCliente().getId()==null) {
			throw new ExcepcionCreacionTarjeta("Debe definir el id del cliente");
		}
		if(datosCreacionTarjeta.getCliente().getTipoId()==null) {
			throw new ExcepcionCreacionTarjeta("Debe definir el tipo de identificación del cliente");
		}
	}
}
