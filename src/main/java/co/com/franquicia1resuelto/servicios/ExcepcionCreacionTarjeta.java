package co.com.franquicia1resuelto.servicios;

public class ExcepcionCreacionTarjeta extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ExcepcionCreacionTarjeta(String mensaje) {
		super(mensaje);
	}

}
