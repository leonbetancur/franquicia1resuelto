
package co.com.franquicia1resuelto;

import javax.jws.WebService;

@WebService(endpointInterface = "co.com.franquicia1resuelto.HelloWorld")
public class HelloWorldImpl implements HelloWorld {

    public String sayHi(String text) {
        return "Hello " + text;
    }
}

